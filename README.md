# vim-config

Make sure to run
```
git submodule update --init --recursive
```
## Syntastic eslint enable
Don't forget to install eslint on your global: ``npm install -g eslint@latest``
And then, set $ESLINT_EXEC environment variable to where eslint bin gets installed to.

## Neovim installation
```
ln -s vim ~/.config/nvim
ln -s vimrc vim/init.vim
```
