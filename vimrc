execute pathogen#infect()
syntax on
filetype plugin indent on
let mapleader=","
set number
set hidden
set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
set ignorecase
set smartcase
set incsearch
set background=dark

colorscheme monokai

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

if $TERM_PROGRAM =~ "iTerm"
    let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
    let &t_EI = "\<Esc>]50;CursorShape=0\x7" " Block in normal mode
endif
"Page up - page down
nmap <Leader>e <C-d>
nmap <Leader>q <C-u>
"Splits
nmap <Leader>w <C-w>w

" Run NeoMake on read and write operations
autocmd! BufReadPost,BufWritePost * Neomake

" YCM
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

" neomake
nmap <Leader><Space>o :lopen<CR>      " open location window
nmap <Leader><Space>c :lclose<CR>     " close location window
nmap <Leader><Space>, :ll<CR>         " go to current error/warning
nmap <Leader><Space>n :lnext<CR>      " next error/warning
nmap <Leader><Space>p :lprev<CR>      " previous error/warning

let g:neomake_serialize = 1
let g:neomake_serialize_abort_on_error = 1
let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_open_list = 1
let g:neomake_javascript_eslint_maker = {
    \ 'args': ['--no-color', '--format', 'compact'],
    \ 'errorformat': '%f: line %l\, col %c\, %m'
    \ }

let g:jsx_ext_required = 0
let g:javascript_plugin_flow = 1

let g:flow#autoclose = 1

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

"Buffergator
let g:buffergator_show_full_directory_path = 0
let g:buffergator_sort_regime = "mru"
let g:buffergator_viewport_split_policy = "R"
let g:buffergator_suppress_keymaps = 1

nmap <leader>b :BuffergatorOpen
nmap <leader>b :BuffergatorOpen<CR>

let NERDTreeQuitOnOpen=1
map <leader>n :NERDTreeToggle
" CTRL-P
map <C-p> :CtrlP<CR>
set wildignore+=*.beam,*/_build/*

map <leader>gb :Gblame<CR>
map <leader>gd :Gdiff<CR>

let g:ctrlp_map = ''
let g:ctrlp_custom_ignore = {
\ 'dir':  '\v[\/]\.(git|hg|svn)$|bower_components|node_modules',
\ 'file': '\.pyc$\|\.pyo$\|\.rbc$|\.rbo$\|\.class$\|\.o$\|\~$\',
\ }

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
nmap <Leader>p :CtrlP
nmap <leader>m :CtrlPBuffer

source ~/.vimrc.after
